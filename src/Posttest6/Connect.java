package Posttest6;

import java.sql.*;
import javax.swing.JOptionPane;

public class Connect {

    private static Connection con;

    public static Connection getConnection() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/posttest?useTimezone=true&serverTimezone=UTC", "root", "");
            
        } catch (SQLException e) {
            
        }
        return con;
    }
}